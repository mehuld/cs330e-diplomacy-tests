#!/usr/bin/env python3

from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_solve



class TestDiplomacy(TestCase):
    def test_diplomacy_solve_1(self):
        r = StringIO(
            'A Madrid Hold\nB Barcelona Move Madrid\nC London Support B')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(),
                         'A [dead]\nB Madrid\nC London\n')

    def test_diplomacy_solve_2(self):
        r = StringIO(
            'A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(),
                         'A [dead]\nB Madrid\nC [dead]\nD Paris\n')

    def test_diplomacy_solve_3(self):
        r = StringIO(
            'A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(),
                         'A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n')

    def test_diplomacy_solve_4(self):
        r = StringIO(
            'A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(),
                         'A [dead]\nB [dead]\nC [dead]\n')
        
if __name__ == '__main__':
    main()

'''#pragma: no cover
 cat TestDiplomacy.tmp
....
----------------------------------------------------------------------
Ran 4 tests in 0.001s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          61      2     42      2    96%   29->exit, 72-73
TestDiplomacy.py      26      0      0      0   100%
--------------------------------------------------------------
TOTAL                 87      2     42      2    97%
 '''